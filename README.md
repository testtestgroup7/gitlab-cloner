# gitlab-cloner

Clone or fetch recursively all GitLab projects within a given namespace, recreating the group/subgroup structure with folders/sub-folders on the filesystem.

## usage
```bash
gitlab-cloner.py --help

Usage: gitlab-cloner.py [options]

Options:
  -h, --help                           show this help message and exit
  -u URL, --url=URL                    base URL of the GitLab instance
  -t TOKEN, --token=TOKEN              API token
  -n NAMESPACE, --namespace=NAMESPACE  namespace to clone
  -p PATH, --path=PATH                 destination path for cloned projects
  --disable-root                       do not create root namepace folder in path
  --dry-run                            list the repositories without clone/fetch
```

## examples

```
./gitlab-cloner.py --path=~/code/ --token=FOOBAR --namespace=gitlab-org
```
